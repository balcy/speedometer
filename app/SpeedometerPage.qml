import QtQuick 2.6
import QtGraphicalEffects 1.0
import Ubuntu.Components 1.3
import QtPositioning 5.2
import QtSensors 5.2
import "util.js" as Util

Page {
    id: speedometerpage

    readonly property string unitlabel: settingspage.unitLabel
    readonly property double scale: settingspage.scale
    property int speed: 0

    function refresh() {

        // m/s, mph or km/h
        speed = geoposition.position.speed === -1 ? speed : (geoposition.position.speed) * scale;

        rotation.angle = (speed * 1.055) + 120.2

        var timestamp_gps = geoposition.position.timestamp ? geoposition.position.timestamp : "";

        infotext.text = "Last GPS update: " + "\n" + timestamp_gps + "\n" + "speed: " + speed + " " + unitlabel;
    }

    onScaleChanged: {

        refresh();
    }

    title: i18n.tr("Speedometer")

    head.actions: [
        Action {
            text: i18n.tr("Br")
            iconSource: Util.getIcon("settings")
            onTriggered: {
                pageStack.push(settingspage)
            }
        }
    ]

    PositionSource {
        id: geoposition
        active: true
        preferredPositioningMethods: PositionSource.SatellitePositioningMethods

        onPositionChanged: {

            overlay.color = "#FF0000";

            gpsUpdateTimer.restart();

            refresh();
        }
    }

    Rectangle {
        anchors.fill: parent
        color: "#101010"
        Text {
            id: infotext
            anchors.fill: parent
            font.pointSize: units.gu(1.5)
            color: "red"
        }
    }

    Rectangle {
        id: tachometer
        anchors.centerIn: parent
        width: parent.width < parent.height ? parent.width * .95 : parent.height * .95
        height: width * 0.97
        color: "transparent"
        radius: width * 0.5

        Image {
            source: Qt.resolvedUrl("graphics/tacho_mono.png")
            anchors.centerIn: parent
            width: parent.width
            height: parent.height
            smooth: true
            antialiasing: true
        }

        Image {
            id: needle
            source: Qt.resolvedUrl("graphics/needle.png")
            //anchors.centerIn: parent
            x: parent.width / 2
            y: parent.height / 2 - 2.5
            width: parent.radius
            smooth: true
            antialiasing: true
            visible: false
        }

        ColorOverlay {
            id: overlay
            anchors.fill: needle
            source: needle
            transform: rotation
            color: "#ff0000" // make image like it lays under red glass
        }

        // circle in the middle of the needle
        Rectangle {
            id: circle
            anchors.centerIn: parent
            color: "#111111"
            border.color: "#222222"
            border.width: 1
            width: 80
            height: width
            radius: width/2
            smooth: true
            antialiasing: true
        }

        Rotation {
            id: rotation
            origin.x: 3
            origin.y: 3
            angle: 121

            Behavior on angle {

                NumberAnimation {
                    duration: 1500
                    easing.type: Easing.OutCubic
                }
            }
        }

        // display of the speed in capital letters
        Text{
           id: speedDisplay
           anchors.bottom: circle.top
           anchors.horizontalCenter: circle.horizontalCenter
           color: "#CCCCCC"
           font.pointSize: 40
           horizontalAlignment: Text.AlignHCenter
           text: unitlabel

        }

        // "watchdog" timer to set the needle inactive,
        Timer {
            id: gpsUpdateTimer
            repeat: false
            running: false
            interval: 3000
            onTriggered: {
              overlay.color = "#808080"
            }
        }
    }
}