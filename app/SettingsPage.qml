import QtQuick 2.6
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0
import QtGraphicalEffects 1.0
import Ubuntu.Components 1.3

Page {
    id: settingspage

    property variant unitScales: [2, 3.6, 1.0]
    property variant unitStrings: ["mph", "km/h", "m/s"]
    property double scale : unitScales[settings.unit]
    property string unitLabel : unitStrings[settings.unit]

    title: i18n.tr("Settings")


    Settings {
        id: settings
        property int unit: 0
    }

    Rectangle {
        anchors.fill: parent
        color: "#101010"
    }

    Column {
        id: settingsColumn

        anchors.fill: parent
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        ListItem {
            id: displayOnItem
            height: displayOnLayout.height
//            divider.visible: true
            ListItemLayout {
                id: displayOnLayout
                title.text: i18n.tr("Keep screen on")
                Switch {
                    id: displayOnSwitch
                    SlotsLayout.position: SlotsLayout.Last
                    checked: settingspage.keepDisplayOn
                    onCheckedChanged: {
                      settingspage.keepDisplayOn = checked;
                    }
                }
            }
                onClicked: displayOnSwitch.checked = !displayOnSwitch.checked;
        }

        //dirty but enable space above OptionSelector
        ListItem{
          id: emptySpace
          height: units.gu(2)
          divider.visible: false
        }

        ListItem{
            id: unitSelectorItem
            height: 300
            divider.visible: false

            ListItemLayout {
                id: unitSelectorLayout
                title.text: i18n.tr("Unit:")
            }

/*            ComboBox {
                id : unitSelector
//                property var values: ['miles per hour', 'kilometers per hour', 'meters per second']
                model: ["miles per hour", "kilometers per hour", "meters per second"]
//                anchors.horizontalCenter: parent.horizontalCenter
                anchors.right: parent.right

//                model: values

                highlightWhenPressed: false

// non existent property                onSelectedIndexChanged: {
                    settings.unit = selectedIndex
                    return false
                }
            }*/

            OptionSelector {
                id: unitSelector
                property var values: ['miles per hour', 'kilometers per hour', 'meters per second']
                //readonly property var currentText: values [this.selectedIndex]

                model: values

                width: unitSelectorLayout.width *  1 / 2
                anchors.right: parent.right

                highlightWhenPressed: false

                onSelectedIndexChanged: {
                    settings.unit = selectedIndex
                    return false
                }

            }

            Component.onCompleted:  {
                unitSelector.selectedIndex = settings.unit
            }

        }

    } //Column

}