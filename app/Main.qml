import QtQuick 2.6
import Ubuntu.Components 1.3
import QtSystemInfo 5.0

/*!
    \brief MainView with a Label and Button elements.
    */
MainView {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: "speedometer.ubuntutouch"
    automaticOrientation: true

    width: units.gu(100)
    height: units.gu(75)

    PageStack {
        id: pageStack

        Component.onCompleted: {
            pageStack.push(speedometerpage)
            Theme.name = 'Ubuntu.Components.Themes.SuruDark'
        }

        SettingsPage {
            id: settingspage
            visible: false
            property bool keepDisplayOn: true
        }

        ScreenSaver {
            id: screen_saver
            screenSaverEnabled: !settingspage.keepDisplayOn
        }

        SpeedometerPage {
            id: speedometerpage
            visible: false
        }

    }
}